﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      //by the garbage collector
      //using (var db = new AdventureWorks2014Entities())
      //{

      //}

      //path to the output file contained in the project
      //var path = @"../../output.txt";
    }
  }
}
